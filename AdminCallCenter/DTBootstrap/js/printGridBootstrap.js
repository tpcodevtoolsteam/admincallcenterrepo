﻿var _TotalIDRegisters;
var _IdTable;
var _IdGridView;
var _objGridJson;
var _objRegister;
var _objLink;
var _objHidden;
var _activeResponsive = true;
var _table;

var _reload = false;
var _lastValueShowRegisters = 20;

var _boolResponsive = false;

var _initLoad = true;
var _Path;
var _Page;
var _jsonExport;
var _Toogle = "fa-toggle-off";

$(document).ready(function () {

    $.ajaxSetup({
        cache: false
    });

    _Path = window.location.pathname;
    _Page = _Path.substring(_Path.lastIndexOf('/') + 1);

    _jsonExport = _Page + "/fileExport";
    _Page = _Page + "/GetData";

    //alert(_Page);

    /***CONTROLES QUE REALIZAN CAMBIOS EN LA VISUALIZACIÓN DEL GRID***/
    var showRegisters = document.getElementsByName(_IdTable + "_length");


    /***SE RECONOCE EL EVENTO DEL CLICK DEL LINK APUNTANDO AL REGISTRO QUE CORRESPONDA***/
    $(document).on("click", ".table>tbody>tr>td>a", function (event) {
        var link = $(this).attr('search');
        var tr = $(this).parents("tr");

        var CellsRegister = [];
        for (var ind = 0; ind < _TotalIDRegisters; ind++) {
            //CellsRegister[ind] = $(tr[0]).find('.ID' + ind).text();
            CellsRegister[ind] = $($(tr[0]).find('td')[_objRegister[ind].IDREGISTER + 1]).html();
        }

        searchRegister(CellsRegister);

    });

    /***EVENTOS CLICKS DE CAMBIOS DE VISUALIZACION DE REGISTROS EN EL GRID***/
    $(document).on("click", ".btnResponsive", function (event) {

        //$('.InResponsive, .Responsive').toggle();
        if (_activeResponsive == true) {
            $('#' + _IdTable).DataTable().destroy();
            $('#' + _IdTable).removeClass('dtr-inline collapsed');
            //BuildDataTable(_IdTable, true);
            _Toogle = "fa-toggle-on";
            _initLoad = true;
            _boolResponsive = true;
            BuildDataTable(_IdTable);
            _activeResponsive = false;

        }
        else {
            $('#' + _IdTable).DataTable().destroy();
            $('#' + _IdTable).removeClass('dtr-inline collapsed');
            //BuildDataTable(_IdTable, false);
            _Toogle = "fa-toggle-off";
            _initLoad = true;
            _boolResponsive = false;
            BuildDataTable(_IdTable);
            _activeResponsive = true;
        }

    });

    $(window).on('resize', function () {
        //_table.draw();
        if (_boolResponsive == true) {
            $('#' + _IdTable).DataTable().columns.adjust().responsive.recalc();
        }
    });


    $(document).on("click", ".table>tbody>tr", function (event) {
        //alert("click");

        $(".table>tbody>tr").each(function () {
            var selectorRows = $(this).children('td');
            $(selectorRows).each(function () {
                $(this).css("background-color", "white");
            });
        });

        var selector = $(this).children('td');
        $(selector).each(function () {
            $(this).css("background-color", "#acbad5");
        });
        //$(this).css("background-color", "#acbad5");
    });

});

/***MOSTRAR GIF LOADING MIENTRAS REALIZA LAS PETICIONES AJAX***/
$body = $("body");
$(document).on({
    ajaxStart: function () { $body.addClass("loading"); },
    ajaxStop: function () {
        $body.removeClass("loading");
        $('#' + _IdTable).DataTable().columns.adjust().responsive.recalc();
    }
});

function PrintGridBK(obj, idTable, idGridView, idRegister, idLink, objHidden) {
    var htmlGrid = "";
    var tdclass = "odd";
    var i;
    var filas;
    var columns;
    var intColumns;
    var encabezados = obj[0];
    var dataIndex = 0;
    var datoFila;
    var bandera = true;

    var indicePosCol;
    var valorColumna;
    var item;
    var swDisplay = true;
    var display;

    //**** COMIENZO CONSTRUCCIÓN TABLE: Declarar ID del Table que tendrá el reporte con sus atributos correspondientes****//
    //htmlGrid = "<table id=\"tblReport\" class=\"table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";
    htmlGrid = "<table id=\"" + idTable + "\" class=\"table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";

    //********* Comienza a Armar encabezados********************//
    htmlGrid += "<thead>";
    htmlGrid += "<tr role=\"row\" style=\"BACKGROUND: #39779c; COLOR: WHITE;\">";
    dataIndex++;

    intColumns = 0;
    for (i in encabezados) {

        for (indicePosCol = 0; indicePosCol < objHidden.length; indicePosCol++) {
            valorColumna = objHidden[indicePosCol];
            for (item in valorColumna) {
                if (valorColumna[item] == intColumns) {
                    swDisplay = false;
                    break;
                }
            }
        }

        if (swDisplay == true) {
            //display = "style=\"display:block !important;\""
            display = "mBlock"
        }
        else {
            //display = "style=\"display:none !important;\""
            display = "mNone"
        }

        if (bandera == true) {
            //htmlGrid += "<th class=\"mostrarH sorting_desc\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + encabezados[i] + ": activate to sort column ascending\" aria-sort=\"descending\" " + display + ">" + encabezados[i] + "</th>";
            htmlGrid += "<th class=\"mostrarH sorting_desc " + display + "\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + encabezados[i] + ": activate to sort column ascending\" aria-sort=\"descending\">" + encabezados[i] + "</th>";
            bandera = false;
        }
        else {
            //htmlGrid += "<th class=\"mostrarH sorting\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + encabezados[i] + ": activate to sort column ascending\" " + display + ">" + encabezados[i] + "</th>";
            htmlGrid += "<th class=\"mostrarH sorting " + display + "\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + encabezados[i] + ": activate to sort column ascending\">" + encabezados[i] + "</th>";
        }

        swDisplay = true;
        intColumns++;

        dataIndex++;
    }
    htmlGrid += "</tr>";
    htmlGrid += "</thead>";
    //********* Fin de armado de encabezados********************//

    bandera = true;

    //********* Comienza a Armar las filas con todos los registros********************//
    htmlGrid += "<tbody>";
    for (filas = 1; filas < obj.length; filas++) {
        htmlGrid += "<tr role=\"row\">";
        datoFila = obj[filas];

        intColumns = 0;
        for (columns in datoFila) {

            /***En caso de que se hayan indicados columnas para el link con su correspondiente ID de Registro (Por Defecto viene en -1)
            Se Procede a consultar y validar dichas columnas para presentarlas en el Grid***/

            for (indicePosCol = 0; indicePosCol < objHidden.length; indicePosCol++) {
                valorColumna = objHidden[indicePosCol];
                for (item in valorColumna) {
                    if (valorColumna[item] == intColumns) {
                        swDisplay = false;
                        break;
                    }
                }
            }

            if (swDisplay == true) {
                //display = "style=\"display:block !important;\""
                display = "mBlock"
            }
            else {
                //display = "style=\"display:none !important;\""
                display = "mNone"
            }

            if (intColumns == idRegister) {
                //htmlGrid += "<td class=\"ID\">";
                htmlGrid += "<td class=\"ID " + display + "\">";
            }
            else {
                //htmlGrid += "<td>";
                htmlGrid += "<td class=\"" + display + "\">";
            }
            if (intColumns == idLink) {
                htmlGrid += "<a search=\"_REPLACE\">" + datoFila[columns] + "</a></td>";
            }
            else {
                htmlGrid += datoFila[columns] + "</td>"
            }

            swDisplay = true;
            intColumns++;

        }

        //}
        htmlGrid += "</tr>";
    }
    htmlGrid += "</tbody>";
    //********* Fin de Armado de las filas con todos los registros********************//

    htmlGrid += "</table>";
    //**** FIN CONSTRUCCIÓN TABLE****//


    document.getElementById(idGridView).innerHTML = htmlGrid;

    var table = $('#' + idTable).DataTable({
        buttons: [{
            extend: 'copy',
            text: '<i class="fa fa-copy fa-lg" aria-hidden="true" data-toggle="tooltip" title="Copiar al Portapapeles"></i>',
            className: 'btn-info'
        },
       {
           extend: 'csvHtml5',
           text: '<i class="fa fa-table fa-lg" aria-hidden="true" data-toggle="tooltip" title="CSV"></i>',
           fieldSeparator: ';',
           className: 'btn-info'
       },
       {
           extend: 'excelHtml5',
           text: ' <i class="fa fa-file-excel-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="Excel"></i>',
           className: 'btn-info'
       },
       {
           extend: 'pdfHtml5',
           orientation: 'landscape',
           pageSize: 'A2',
           text: '<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="PDF"></i>',
           className: 'btn-info'
       },
       {
           extend: 'colvis',
           columns: ':gt(0)',
           text: ' <i class="fa fa-eye fa-lg" aria-hidden="true" data-toggle="tooltip" title="Visualizar Columnas"></i>',
           className: 'btn-info'
       },
        ],
        "lengthMenu": [[20, 50, 100, 250], [20, 50, 100, 250]],
        aaSorting: [],
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros",
            "zeroRecords": "No se encontraron resultados",
            //"info": "Mostrando Página _PAGE_ de _PAGES_",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            //"infoEmpty": "No hay información del reporte",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },
    });


    $($(".row:eq(0)").children()[0]).removeClass("col-sm-6");
    $($(".row:eq(0)").children()[0]).addClass("col-sm-2");

    $("<div class='col-sm-4'></div>").insertBefore('#' + idTable + '_wrapper .col-sm-6:eq(0)');

    table.buttons().container()
    .appendTo('#' + idTable + '_wrapper .col-sm-4:eq(0)');

    $(".btn-info").css("background-color", "#39779c");
    $(".btn-info").css("border-color", "#39779c");
    $(".btn-info").css("margin-bottom", "5px");

    $(".mNone").css("display", "none");

    return htmlGrid;
}

function PrintGridBK2(obj, idTable, idGridView, objRegister, objLink, objHidden) {
    var htmlGrid = "";
    var tdclass = "odd";
    var filas;
    var columns;
    var intColumns;
    var encabezados = obj[0];
    var dataIndex = 0;
    var datoFila;
    var bandera = true;

    var indicePosCol;
    var valorColumna;
    var item;
    var swDisplay = true;
    var swRegister = true;
    var intRegister = 0;
    var intLink = 0;
    var swLink = true;
    var display;
    var register = "";

    var NoColumnsVis = [];
    var ColumnsVis = [];
    var intColumnsVis = 0;

    _objGridJson = obj;
    _objRegister = objRegister;
    _objLink = objLink;
    _objHidden = objHidden;

    //**** COMIENZO CONSTRUCCIÓN TABLE: Declarar ID del Table que tendrá el reporte con sus atributos correspondientes****//
    //htmlGrid = "<table id=\"tblReport\" class=\"table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";
    htmlGrid = "<table id=\"" + idTable + "\" class=\"table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";

    //********* Comienza a Armar encabezados********************//
    htmlGrid += "<thead>";
    htmlGrid += "<tr role=\"row\" style=\"BACKGROUND: #39779c; COLOR: WHITE;\">";
    dataIndex++;

    intColumns = 0;
    var index = [];
    var indice = 0;
    for (i in encabezados) {

        index.push(i)

        for (indicePosCol = 0; indicePosCol < objHidden.length; indicePosCol++) {
            valorColumna = objHidden[indicePosCol];
            for (item in valorColumna) {
                if (valorColumna[item] == intColumns) {
                    swDisplay = false;
                    break;
                }
            }
        }

        if (swDisplay == true) {
            display = "mBlock"
        }
        else {
            display = "mNone"
        }

        for (indicePosCol = 0; indicePosCol < objRegister.length; indicePosCol++) {
            valorColumna = objRegister[indicePosCol];
            for (item in valorColumna) {
                if (valorColumna[item] == intColumns) {
                    swRegister = false;
                    break;
                }
            }
            if (swRegister == false) {
                break;
            }
        }

        if (swRegister == false || swDisplay == false) {
            register = "NoVisual"
        }
        else {
            register = "Visual"
        }

        if (bandera == true) {
            //htmlGrid += "<th class=\"" + register + " mostrarH sorting_desc " + display + "\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + encabezados[i] + ": activate to sort column ascending\" aria-sort=\"descending\">" + encabezados[i] + "</th>";
            htmlGrid += "<th class=\"" + register + " mostrarH sorting_desc " + display + "\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + index[intColumns] + ": activate to sort column ascending\" aria-sort=\"descending\">" + index[intColumns] + "</th>";
            bandera = false;
        }
        else {
            //htmlGrid += "<th class=\"" + register + " mostrarH sorting " + display + "\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + encabezados[i] + ": activate to sort column ascending\">" + encabezados[i] + "</th>";
            htmlGrid += "<th class=\"" + register + " mostrarH sorting " + display + "\" data-index=" + dataIndex + " tabindex=\"0\" aria-controls=\"example\" rowspan=\"1\" colspan=\"1\" aria-label=\"" + index[intColumns] + ": activate to sort column ascending\">" + index[intColumns] + "</th>";
        }

        swDisplay = true;
        swRegister = true
        intColumns++;

        dataIndex++;
    }
    htmlGrid += "</tr>";
    htmlGrid += "</thead>";
    //********* Fin de armado de encabezados********************//

    bandera = true;
    swDisplay = true;
    swLink = true

    //********* Comienza a Armar las filas con todos los registros********************//
    htmlGrid += "<tbody>";
    for (filas = 1; filas < obj.length; filas++) {
        htmlGrid += "<tr role=\"row\">";
        datoFila = obj[filas];

        intColumns = 0;
        for (columns in datoFila) {

            /***En caso de que se hayan indicados columnas para el link con su correspondiente ID de Registro (Por Defecto viene en -1)
            Se Procede a consultar y validar dichas columnas para presentarlas en el Grid***/

            for (indicePosCol = 0; indicePosCol < objHidden.length; indicePosCol++) {
                valorColumna = objHidden[indicePosCol];
                for (item in valorColumna) {
                    if (valorColumna[item] == intColumns) {
                        swDisplay = false;
                        break;
                    }
                }
            }

            if (swDisplay == true) {
                //display = "style=\"display:block !important;\""
                display = "mBlock"
            }
            else {
                //display = "style=\"display:none !important;\""
                display = "mNone"
            }

            for (indicePosCol = 0; indicePosCol < objRegister.length; indicePosCol++) {
                valorColumna = objRegister[indicePosCol];
                for (item in valorColumna) {
                    if (valorColumna[item] == intColumns) {
                        swRegister = false;
                        break;
                    }
                }
                if (swRegister == false) {
                    break;
                }
            }

            if (swRegister == false || swDisplay == false) {
                register = "NoVisual"
            }
            else {
                register = "Visual"
            }

            if (swRegister == false) {
                htmlGrid += "<td class=\"" + register + " ID" + intRegister + " " + display + "\">";
                intRegister++;
            }
            else {
                htmlGrid += "<td class=\"" + register + " " + display + "\">";
            }

            for (indicePosCol = 0; indicePosCol < objLink.length; indicePosCol++) {
                valorColumna = objLink[indicePosCol];
                for (item in valorColumna) {
                    if (valorColumna[item] == intColumns) {
                        swLink = false;
                        break;
                    }
                }
                if (swLink == false) {
                    break;
                }
            }

            if (swLink == false) {
                htmlGrid += "<a search=\"_REPLACE\">" + datoFila[columns] + "</a></td>";

            }
            else {
                htmlGrid += datoFila[columns] + "</td>"
            }

            swRegister = true
            swLink = true
            swDisplay = true;
            intColumns++;

        }

        intRegister = 0;
        _TotalIDRegisters = objRegister.length;
        htmlGrid += "</tr>";
    }
    htmlGrid += "</tbody>";
    //********* Fin de Armado de las filas con todos los registros********************//

    htmlGrid += "</table>";
    //**** FIN CONSTRUCCIÓN TABLE****//

    for (indicePosCol = 0; indicePosCol < objRegister.length; indicePosCol++) {
        valorColumna = objRegister[indicePosCol];
        NoColumnsVis[indicePosCol] = indicePosCol;
    }

    indicePosCol = 0
    var ColumnsVis = [];
    for (var i = 0; i < intColumns; i++) {
        if (NoColumnsVis[i] != i) {
            ColumnsVis[indicePosCol] = i;
            indicePosCol++;
        }
    }

    _IdTable = idTable;
    _IdGridView = idGridView;

    document.getElementById(idGridView).innerHTML = htmlGrid;

    BuildDataTable(idTable, true);

    return htmlGrid;
}

function PrintGrid(obj, idTable, idGridView, objRegister, objLink, objHidden, objColumns) {
    var htmlGrid = "";
    var tdclass = "odd";
    var filas;
    var columns;
    var intColumns;
    var encabezados = obj[0];
    var dataIndex = 0;
    var datoFila;
    var bandera = true;

    var indicePosCol;
    var valorColumna;
    var item;
    var swDisplay = true;
    var swRegister = true;
    var intRegister = 0;
    var intLink = 0;
    var swLink = true;
    var display;
    var register = "Visual";

    var NoColumnsVis = [];
    var ColumnsVis = [];
    var intColumnsVis = 0;

    var displayInitColumn = false;

    _objGridJson = obj;
    _objRegister = objRegister;
    _objLink = objLink;
    _objHidden = objHidden;

    var classRegister;
    var classLink;

    //**** COMIENZO CONSTRUCCIÓN TABLE: Declarar ID del Table que tendrá el reporte con sus atributos correspondientes****//
    //htmlGrid = "<table id=\"tblReport\" class=\"table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";

    //htmlGrid = "<table id=\"" + idTable + "\" clientidmode=\"Static\" class=\" hover table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";
    //htmlGrid = "<table id=\"" + idTable + "\" class=\"hover table table-bordered dt-responsive display\" cellspacing=\"0\" width=\"100%\">";

    htmlGrid = "<table id=\"" + idTable + "\" class=\"hover table table-bordered dt-responsive display order-column\" cellspacing=\"0\" width=\"100%\">";

    //********* Comienza a Armar encabezados********************//
    htmlGrid += "<thead>";
    //htmlGrid += "<tr class=\"gridStyle\" role=\"row\" style=\"BACKGROUND: #39779c; COLOR: WHITE;\">";
    htmlGrid += "<tr>";
    dataIndex++;

    intColumns = 0;
    var index = [];
    var indice = 0;

    //htmlGrid += "<th class=\"dt-center\" style=\"width:25px;\"></th>";
    //htmlGrid += "<th class=\"dt-center\" width=25px style=\"width:25px;\"></th>";
    //htmlGrid += "<th class=\"dt-center\" width=10%></th>";

    htmlGrid += "<th class=\"dt-center\" style=\"max-width:25px;width:25px;\"></th>";
    //htmlGrid += "<th class=\"dt-center\"></th>";

    intColumns = 0;
    for (i in encabezados) {

        index.push(i)

        for (indicePosCol = 0; indicePosCol < objHidden.length; indicePosCol++) {
            valorColumna = objHidden[indicePosCol];
            for (item in valorColumna) {
                if (valorColumna[item] == intColumns) {
                    swDisplay = false;
                    break;
                }
            }
            if (swDisplay == false) {
                break;
            }
        }

        for (indicePosCol = 0; indicePosCol < objRegister.length; indicePosCol++) {
            valorColumna = objRegister[indicePosCol];
            for (item in valorColumna) {
                if (valorColumna[item] == intColumns) {
                    swRegister = false;
                    break;
                }
            }
            if (swRegister == false) {
                break;
            }
        }

        for (indicePosCol = 0; indicePosCol < objLink.length; indicePosCol++) {
            valorColumna = objLink[indicePosCol];
            for (item in valorColumna) {
                if (valorColumna[item] == intColumns) {
                    swLink = false;
                    break;
                }
            }
            if (swLink == false) {
                break;
            }
        }

        if (swDisplay == true) {
            register = "Visual"
        }
        else {
            register = "NoVisual"
        }

        if (swLink == false) {
            //htmlGrid += "<a search=\"_REPLACE\">" + datoFila[columns] + "</a></td>";
            classLink = "LINK"
        }
        else {
            //htmlGrid += datoFila[columns] + "</td>"
            classLink = "NOLINK"
        }

        if (swRegister == false) {
            classRegister = "REG"
        }
        else {
            classRegister = "NOREG"
        }

        //if (index == 2) {
        //    htmlGrid += "<th style=\"width:30%;\">" + index[intColumns] + "</th>";
        //}
        //else {
            htmlGrid += "<th>" + index[intColumns] + "</th>";
        //}

        swDisplay = true;
        swLink = true;
        swRegister = true;
        intColumns++;

        dataIndex++;
    }
    htmlGrid += "</tr>";
    htmlGrid += "</thead>";
    //********* Fin de armado de encabezados********************//

    htmlGrid += "<tbody></tbody>";

    htmlGrid += "</table>";
    //**** FIN CONSTRUCCIÓN TABLE****//

    _IdTable = idTable;
    _IdGridView = idGridView;
    _TotalIDRegisters = objRegister.length;

    document.getElementById(idGridView).innerHTML = htmlGrid;

    $("#columns").val(objColumns);
    BuildDataTable(idTable);

    return htmlGrid;
}

function BuildDataTableBK(idTable, boolResponsive) {

    if (boolResponsive == undefined) {
        boolResponsive = true
    }

    //var table = $('#' + idTable).DataTable({
    _table = $('#' + idTable).DataTable({
        //dom: 'Zlfrtip',
        //"sDom": 'Rplfrti',
        //"oColReorder": {
        //    "headerContextMenu": true
        //},
        buttons: [{
            extend: 'copy',
            //exportOptions: {
            //    columns: ColumnsVis
            //},
            text: '<i class="fa fa-copy fa-lg" aria-hidden="true" data-toggle="tooltip" title="Copiar al Portapapeles"></i>',
            className: 'btn-info'
        },
       {
           extend: 'csvHtml5',
           text: '<i class="fa fa-table fa-lg" aria-hidden="true" data-toggle="tooltip" title="CSV"></i>',
           fieldSeparator: ';',
           className: 'btn-info'
       },
       {
           extend: 'excelHtml5',
           text: ' <i class="fa fa-file-excel-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="Excel"></i>',
           className: 'btn-info'
       },
       {
           extend: 'pdfHtml5',
           orientation: 'landscape',
           pageSize: 'A2',
           text: '<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="PDF"></i>',
           className: 'btn-info'
       },
       {
           extend: 'colvis',
           columns: '.Visual',
           text: ' <i class="fa fa-eye fa-lg" aria-hidden="true" data-toggle="tooltip" title="Visualizar Columnas"></i>',
           className: 'btn-info'
       },
       {
           //extend: 'responsive',
           text: ' <i class="Responsive fa fa-toggle-on fa-lg active" aria-hidden="true" data-toggle="tooltip" title="Activar/Desactivar Responsive"></i>',
           className: 'btn-info btnResponsive'
       },
        ],
        "lengthMenu": [[20, 50, 100, 250], [20, 50, 100, 250]],
        aaSorting: [],
        "fnDrawCallback": function (oSettings, json) {
            $(".mNone").css("display", "none");
            //test();
        },
        //"autoWidth": false,
        "scrollX": true,
        "scrollY": true,
        responsive: boolResponsive,
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros",
            "zeroRecords": "No se encontraron resultados",
            //"info": "Mostrando Página _PAGE_ de _PAGES_",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            //"infoEmpty": "No hay información del reporte",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            //"processing": "<div class='loader'></div>",
            //"loadingRecords": "<div class='loader'></div>",
            //processing: "<img src='http://i.stack.imgur.com/FhHRx.gif'> Loading...",
            sLoadingRecords: '<span style="width:100%;"><img src="http://www.snacklocal.com/images/ajaxload.gif"></span>'
        },

    });

    //new $.fn.dataTable.FixedColumns(table);

    $($(".row:eq(0)").children()[0]).removeClass("col-sm-6");
    $($(".row:eq(0)").children()[0]).addClass("col-sm-2");

    $("<div class='col-sm-4'></div>").insertBefore('#' + idTable + '_wrapper .col-sm-6:eq(0)');

    //table.buttons().container()
    _table.buttons().container()
    .appendTo('#' + idTable + '_wrapper .col-sm-4:eq(0)');

    $(".btn-info").css("background-color", "#39779c");
    $(".btn-info").css("border-color", "#39779c");
    $(".btn-info").css("margin-bottom", "5px");

    $(".mNone").css("display", "none");

    /***SELECCIONAR DATOS DE LOS CONTROLES EN EL GRID***/
    var showRegisters = document.getElementsByName(_IdTable + "_length");


    $(showRegisters).val(_lastValueShowRegisters);

}

function BuildDataTable(idTable) {

    var linkData = true;
    var descriptionColumns = $("#columns").val();
    //var jsonColumns = $.parseJSON(descriptionColumns);
    var jsonColumns = $.parseJSON(descriptionColumns);
    var args = "data, type, full, meta";

    $.each(jsonColumns, function (key, value) {
        var selector = $(this);
        $.each(value, function (key, value) {
            if (key=="render") {
                selector[0].render = new Function(args, value);
            }
        });
    });

    
    //jsonColumns.render();

    if (_boolResponsive == undefined) {
        _boolResponsive = false;
    }

    var table = $('#' + idTable).DataTable({
        //dom: 'lfrtip',
        buttons: [{
            extend: 'copyHtml5',
            text: '<i class="fa fa-copy fa-lg" aria-hidden="true" data-toggle="tooltip" title="Copiar al Portapapeles"></i>',
            className: 'btn-info'
        },
       {
           extend: 'csvHtml5',
           text: '<i class="fa fa-table fa-lg" aria-hidden="true" data-toggle="tooltip" title="CSV"></i>',
           fieldSeparator: ';',
           charset: 'UTF-16LE',
           bom: true,
           className: 'btn-info'
       },
       {
           extend: 'excelHtml5',
           text: ' <i class="fa fa-file-excel-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="Excel"></i>',
           className: 'btn-info'
       },
       //{
       //    extend: 'pdfHtml5',
       //    orientation: 'landscape',
       //    pageSize: 'A2',
       //    text: '<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="PDF"></i>',
       //    className: 'btn-info'
       //},
       {
           extend: 'colvis',
           columns: '.Visual',
           text: ' <i class="fa fa-eye fa-lg" aria-hidden="true" data-toggle="tooltip" title="Visualizar Columnas"></i>',
           className: 'btn-info'
       },
       {
           text: ' <i class="Responsive fa ' + _Toogle + ' fa-lg active" aria-hidden="true" data-toggle="tooltip" title="Activar/Desactivar Responsive"></i>',
           className: 'btn-info btnResponsive'
       },
        ],
        "fnDrawCallback": function (oSettings, json) {
            var headers = _objGridJson[0];
            for (var ind = 0; ind < _objHidden.length; ind++) {
                $(".table td:nth-child(" + ((_objHidden[ind].HIDDEN) + 2) + ")").removeClass("Visual");
                $(".table>thead>tr>th:eq(" + ((_objHidden[ind].HIDDEN) + 1) + ")").removeClass("Visual");
                $(".table td:nth-child(" + ((_objHidden[ind].HIDDEN) + 2) + ")").addClass("NoVisual");
                $(".table>thead>tr>th:eq(" + ((_objHidden[ind].HIDDEN) + 1) + ")").addClass("NoVisual");
            }

            if ((_boolResponsive == false) && _objLink.length == 0) {
                $(".table td:nth-child(1)").removeClass("column0Vis");
                $(".table>thead>tr>th:eq(0)").removeClass("column0Vis");
                $(".table td:nth-child(1)").addClass("NoVisual");
                $(".table>thead>tr>th:eq(0)").addClass("NoVisual");
            }
            else {
                $(".table td:nth-child(1)").removeClass("NoVisual");
                $(".table>thead>tr>th:eq(0)").removeClass("NoVisual");
                $(".table td:nth-child(1)").addClass("column0Vis");
                $(".table>thead>tr>th:eq(0)").addClass("column0Vis");
            }

            $(".NoVisual").css("display", "none");

            $(".table>thead>tr>th:eq(0)").removeClass("sorting_asc");
            $(".table>thead>tr>th:eq(0)").addClass("dt-center");
            
            $(".table>tbody>tr>td").css("vertical-align", "middle");

            if (_objRegister.length > 0) {
                $(".table>tbody>tr").each(function () {
                    var selector = $(this);
                    var childrenArray = $(selector).children('td');

                    for (var ind = 0; ind < _objRegister.length; ind++) {
                        if ($.trim($(childrenArray[(_objRegister[ind].IDREGISTER) + 1]).text()) == '') {
                            linkData = false;
                            break;
                        }
                    }
                    if (linkData == false) {
                        $(childrenArray[0]).html("");
                        linkData = true;
                    }
                });
            }

            
        },

        "scrollY": "78vh",
        //"scrollY": "100vh",
        //"scrollY": "100%",
        "scrollX": "100%",
        "scrollCollapse": true,
        //"scrollCollapse": false,

        responsive: _boolResponsive,
        //responsive: false,

        "order": [[0, "asc"]],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": _Page,
        "fnServerData": function (sSource, aoData, fnCallback) {
            aoData.push({ "name": "roleId", "value": "admin" });
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success": function (msg) {
                    var json = $.parseJSON(msg.d);
                    fnCallback(json);
                    $("#" + _IdTable).show();

                    if (_initLoad == true) {
                        _initLoad = false;

                        $($(".row:eq(0)").children()[0]).removeClass("col-sm-6");
                        $($(".row:eq(0)").children()[0]).addClass("col-sm-2");

                        $("<div class='col-sm-4'></div>").insertBefore('#' + idTable + '_wrapper .col-sm-6:eq(0)');

                        table.buttons().container()
                        .appendTo('#' + idTable + '_wrapper .col-sm-4:eq(0)');

                        //$(".btn-info").css("background-color", "#39779c");
                        //$(".btn-info").css("border-color", "#39779c");

                        //$(".btn-group").css("margin-bottom", "-25px");
                    }
                    
                    for (var ind = 0; ind < _objHidden.length; ind++) {
                        $(".table td:nth-child(" + ((_objHidden[ind].HIDDEN) + 2) + ")").removeClass("Visual");
                        $(".table>thead>tr>th:eq(" + ((_objHidden[ind].HIDDEN) + 1) + ")").removeClass("Visual");
                        $(".table td:nth-child(" + ((_objHidden[ind].HIDDEN) + 2) + ")").addClass("NoVisual");
                        $(".table>thead>tr>th:eq(" + ((_objHidden[ind].HIDDEN) + 1) + ")").addClass("NoVisual");
                    }

                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });

        },
        select: true,
        "columns": jsonColumns,
        //fixedColumns: true,
        "autoWidth": true,
        //fixedColumns: true,
        //"autoWidth": false,
        "iDisplayLength": 50,
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            buttons: {
                copyTitle: 'Copiado al Portapapeles',
                copySuccess: {
                    1: "Se copió 1 registro al portapapeles",
                    _: "Se copiaron %d registros al portapapeles"
                },
            },
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            searchPlaceholder: "Buscar registro"
        },

    });



    _table = table

    //$('#' + _IdTable).DataTable().columns.adjust().responsive.recalc();

    //table.draw();
    //table.columns.adjust().draw();

    //$($(".row:eq(0)").children()[0]).removeClass("col-sm-6");
    //$($(".row:eq(0)").children()[0]).addClass("col-sm-2");

    //$("<div class='col-sm-4'></div>").insertBefore('#' + idTable + '_wrapper .col-sm-6:eq(0)');

    //_table.buttons().container()
    //.appendTo('#' + idTable + '_wrapper .col-sm-4:eq(0)');

    //$(".btn-info").css("background-color", "#39779c");
    //$(".btn-info").css("border-color", "#39779c");
    //$(".btn-group").css("margin-bottom", "5px");

    $.fn.DataTable.Api.register('buttons.exportData()', function (options) {
        var dataex = "";

        try {
            $.ajax({
                url: _jsonExport,
                data: "{}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data != undefined) {
                        if (data.d != undefined) {
                            dataex = $.parseJSON(data.d.objJson);
                        }
                        else {
                            alert("Error Consultando Información");
                        }
                    }
                    else {
                        alert("Error Consultando Información");
                    }
                },
                error: function (xhr) {
                    //msgAlert("error", "Error", "Error Consultando Información");
                    alert("Error Consultando Información");
                    //msgAlert("error", "Error", 'Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                },
                async: false
            });
        }
        catch (err) {
            //msgAlert("error", "Error", err.message);
            alert("Error Consultando Información: " + err.message);
        }

        var header=[];

            $("#" + _IdTable + " thead tr th div").each(function () {
                header.push($($(this)[0]).html());
            })

        header = header.filter(function (v) { return v !== '' });

        var data = {
            header: header,
            body: []
        };

        for (var i = 0; i < dataex.length; ++i) {
        //for (var i = 1; i < dataex.length; ++i) {
            var rowData = dataex[i];
            var rowDataForExport = [];
            // Get only data of property which display in table
            for (var idx = 0; idx < header.length; ++idx) {
            //for (var idx = 1; idx < header.length; ++idx) {
                var cellData = rowData[header[idx]];
                if (cellData!=undefined) {
                    rowDataForExport.push(cellData);
                }
            }
            data.body.push(rowDataForExport);
        }

        return data;
        //}
    });
}