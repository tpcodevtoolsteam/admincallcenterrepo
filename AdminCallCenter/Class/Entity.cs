﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCallCenter.Class
{
    /// <summary>
    /// Class 
    /// </summary>
    public class Entity
    {
    }

    public class DataResult
    {
        public decimal AverageAge;
        public int CountAge20;
        public int CountAge50;
        public IEnumerable<AdminCallCenter.DataBase.Client> clients;
    }

    public class LoginUser
    {
        public string loginUser;
    }

    public class Template
    {
        public string idTemplate;
    }

    public class Branch
    {
        public string IdValue;
        public string Value;
    }

    public class TemplateAdmin
    {
        public int IdTemplate { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public int IsActive { get; set; }
        public string Body { get; set; }
        public DateTime DateUpate { get; set; }
        public string Login_USER { get; set; }
        public string FileName { get; set; }
    }

    public class TemplateLog
    {
        public int Id_LogTemplate { get; set; }
        public int IdTemplate { get; set; }
        public DateTime SaveDate { get; set; }
        public string Login_USER { get; set; }
        public string Body { get; set; }
        public string FileName { get; set; }
    }

    public class User
    {
        public int Id_USER { get; set; }
        public string Login_USER { get; set; }
        public string FirstName_USER { get; set; }
        public string LastName_USER { get; set; }
        public string Initials_USER { get; set; }
        public string Id_PROF { get; set; }
        public int IsActive_USER { get; set; }
        public int Superior_Id_USER { get; set; }
        public int IsGroup_USER { get; set; }
        public string Email_USER { get; set; }
        public string Team_Id_BRAN { get; set; }
        public string Id_FUNC { get; set; }
        public string Area_Id_BRAN { get; set; }
        public string Phone_USER { get; set; }
        public string Mobile_USER { get; set; }
        public int Typist_Id_USER { get; set; }
        public string Id_CALE { get; set; }
        public string IdTimeZone_USER { get; set; }
        public string Id_SCHE { get; set; }
        public string Id_AUTM { get; set; }
        public int HasWebAccess_USER { get; set; }
        public string DefaultLanguage_USER { get; set; }
        public int IsSystemRecord_USER { get; set; }
    }
        
}