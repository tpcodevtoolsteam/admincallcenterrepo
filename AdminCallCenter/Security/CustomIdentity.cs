﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

namespace AdminCallCenter.Security
{
    public class CustomIdentity : GenericIdentity
    {
        private string _fullName;
        private string _HTMLMenus;
        private string _userName;

        public CustomIdentity(string userName, string fullName) : base(userName)
        {
            _userName = userName;
            _fullName = fullName;
            _HTMLMenus = string.Empty;
        }

        public CustomIdentity(string userName, string fullName, string HTMLUserMenus) : base(userName)
        {
            _userName = userName;
            _fullName = fullName;
            _HTMLMenus = HTMLUserMenus;
        }

        public bool HasIdentity
        {
            get { return !string.IsNullOrWhiteSpace(_fullName); }
        }

        public string FullName
        {
            get { return _fullName; }
        }

        public static CustomIdentity EmptyIdentity
        {
            get { return new CustomIdentity("", ""); }
        }

        public string HTMLMenus
        {
            get { return _HTMLMenus; }
        }

        public string userName
        {
            get { return _userName; }
        }
    }
}