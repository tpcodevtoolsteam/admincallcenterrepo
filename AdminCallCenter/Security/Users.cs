﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Teleperformance.Helpers.Logger;

namespace AdminCallCenter.Security
{
    public class Users
    {
        public Users()
        {
        }

     
        public bool AuthenticateUser(string username, string password, out string resultMessage)
        {
            try
            {
                LAPWeb.UserSoapClient wsLogin = new LAPWeb.UserSoapClient("UserSoap");
                bool isAuthenticated = false;
                string fullName = "";
                resultMessage = "";

                if (wsLogin.AuthenticateUser(username, password))
                {
                    fullName = wsLogin.GetFullName(username);
                    isAuthenticated = true;
                    resultMessage = "inicio de sesión correcto.";
                    CustomAuthentication.SetAuthCookies(username, fullName);

                    //if (userLogged != null)
                    //{
                    //    if (userLogged.Active)
                    //    {
                    //        CustomAuthentication.SetAuthCookies(username, fullName);
                    //        LoadUserMenus(username);
                    //        userLogged.LastLogin = DateTime.Now;
                    //        context.SaveChanges();
                    //        resultMessage = "inicio de sesión correcto.";
                    //        isAuthenticated = true;
                    //        LoggerManager.Logger.UserLogin = username;
                    //    }
                    //    else
                    //    {
                    //        resultMessage = "Autenticación válida, usuario inactivo en el aplicativo.";
                    //        isAuthenticated = false;
                    //    }
                    //}
                    //else
                    //{
                    //    resultMessage = "Autenticación válida, usuario sin permisos de inicio de sesión en el aplicativo.";
                    //    isAuthenticated = false;
                    //}
                }
                else
                {
                    resultMessage = "Nombre de usuario o contraseña incorrecta.";
                }
                return isAuthenticated;
            }
            catch (Exception ex)
            {
                resultMessage = "Excepción al inicio de sesión: " + ex.Message;
                return false;
            }
        }
    }
}