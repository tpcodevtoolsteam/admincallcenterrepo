﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace System.Web
{
    public static class TableHelperExtensions
    {
        public static string BuildTr(object _obj)
        {
            var properties = _obj.GetType().GetProperties();
            var tr = "<tr>";

            foreach (var property in properties)
            {
                tr += String.Format("<td>{0}</td>", property.GetValue(_obj));
            }

            tr += "</tr>";

            return (tr);
        }

        public static string BuildTrHeader(object _obj)
        {
            var properties = _obj.GetType().GetProperties();
            var tr = "<tr>";

            foreach (var property in properties)
            {
                tr += String.Format("<th>{0}</th>", property.Name);
            }

            tr += "</tr>";

            return (tr);
        }


        public static HtmlString BuildTableSimple(this HtmlHelper helper, DataTable dt)
        {
            //Building an HTML string.
            StringBuilder html = new StringBuilder();

            //Table start.
            html.Append("<table  class='table table-bordered table-striped'>");

            //Building the Header row.
            html.Append("<thead><tr>");
            foreach (DataColumn column in dt.Columns)
            {
                html.Append("<th>");
                html.Append(column.ColumnName);
                html.Append("</th>");
            }
            html.Append("</tr></thead>");

            html.Append("<tbody>");

            //Building the Data rows.
            foreach (DataRow row in dt.Rows)
            {
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<td>");
                    html.Append(row[column.ColumnName]);
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }

            html.Append("</tbody>");

            //Table end.
            html.Append("</table>");

            return new HtmlString(html.ToString());
        }

        public static HtmlString BuildTableSimpleWithFooter(this HtmlHelper helper, DataTable dt,int rowFooter)
        {
            //Building an HTML string.
            StringBuilder html = new StringBuilder();

            //Table start.
            html.Append("<table  class='table table-bordered table-striped'>");

            //Building the Header row.
            html.Append("<thead><tr>");
            foreach (DataColumn column in dt.Columns)
            {
                html.Append("<th>");
                html.Append(column.ColumnName);
                html.Append("</th>");
            }
            html.Append("</tr></thead>");

            html.Append("<tbody>");

            //Building the Data rows.
            foreach (DataRow row in dt.Rows)
            {
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<td>");
                    html.Append(row[column.ColumnName]);
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }

            html.Append("</tbody>");

            html.Append("<tfoot>");

            for (int i = 0; i < rowFooter; i++)
            {
                html.Append("<tr>");

                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<th>");
                    //html.Append(column.ColumnName);
                    html.Append("</th>");
                }

                html.Append("</tr>");
            }

            html.Append("</tfoot>");
            

            //Table end.
            html.Append("</table>");

            return new HtmlString(html.ToString());
        }

        public static HtmlString BuildTableDetail(this HtmlHelper helper, DataTable dt)
        {
            //Building an HTML string.
            StringBuilder html = new StringBuilder();

            //Table start.
            html.Append("<table  class='table table-bordered table-striped' width='100%'>");

            //Building the Header row.
            html.Append("<thead><tr>");

            html.Append("<th>-</th>");

            foreach (DataColumn column in dt.Columns)
            {
                html.Append("<th>");
                html.Append(column.ColumnName);
                html.Append("</th>");
            }
            html.Append("</tr></thead>");

            html.Append("<tbody>");

            //Building the Data rows.
            foreach (DataRow row in dt.Rows)
            {
                html.Append("<tr><td><button class='btn btn-success btn-xs btn-info' style='padding:3px 8px;border-bottom:0px;' title='Reporte seguimiento'><i class='fa fa-search-plus'</i></button></td>");

                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<td>");
                    html.Append(row[column.ColumnName]);
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }

            html.Append("</tbody>");

            //Table end.
            html.Append("</table>");

            return new HtmlString(html.ToString());
        }

        static bool IsCollection(Type type)
        {
            return type.GetInterface(typeof(IEnumerable<>).FullName) != null;
        } 
    }
}