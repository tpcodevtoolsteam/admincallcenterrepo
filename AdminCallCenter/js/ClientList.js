﻿var _TotalIDRegisters;
var _IdTable;
var _IdGridView;
var _objGridJson;
var _objRegister;
var _objLink;
var _objHidden;
var _activeResponsive = true;
var _table;

var _reload = false;
var _lastValueShowRegisters = 20;

var _boolResponsive = false;
var _nowrap = "nowrap";

var _initLoad = true;
var _Path;
var _Page;
var _jsonExport;
var _Toogle = "fa-toggle-off";

var _loginUser;


var age = 0;
var now = new Date();
var past;

var nowYear = now.getFullYear();
var pastYear;

var averageAge = 0;
var countAge20 = 0;
var countAge50 = 0;

var data;

$(document).ready(function () {

    _loginUser = $('#loginUser').val();

    $.ajaxSetup({
        cache: false
    });

    /***EVENTOS CLICKS DE CAMBIOS DE VISUALIZACION DE REGISTROS EN EL GRID***/

    $(window).on('resize', function () {
        //_table.draw();
        if (_boolResponsive == true) {
            $('#' + _IdTable).DataTable().columns.adjust().responsive.recalc();
        }
    });


    $(document).on("click", ".table>tbody>tr", function (event) {
        //alert("click");

        $(".table>tbody>tr").each(function () {
            var selectorRows = $(this).children('td');
            $(selectorRows).each(function () {
                $(this).css("background-color", "white");
            });
        });

        var selector = $(this).children('td');
        $(selector).each(function () {
            $(this).css("background-color", "#acbad5");
        });
        //$(this).css("background-color", "#acbad5");
    });

    getClientList("tblClientList", "grdClientList");


    $.fn.DataTable.Api.register('buttons.exportData()', function (options) {
        var dataex = "";

        try {
            $.ajax({
                //url: "/Question/fileExport",
                url: $.url("ClientsFilter/fileExport"),
                data: "{}",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data != undefined) {
                        if (data.objJson != "") {
                            dataex = $.parseJSON(data.objJson);
                        }
                        else {
                            window.error("Error Consultando Información");
                            //alert("Error Consultando Información");
                        }
                    }
                    else {
                        //alert("Error Consultando Información");
                        window.error("Error Consultando Información");
                    }
                },
                error: function (xhr) {
                    //msgAlert("error", "Error", "Error Consultando Información");
                    //alert("Error Consultando Información");
                    window.error("Error Consultando Información");
                    //msgAlert("error", "Error", 'Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                },
                async: false
            });
        }
        catch (err) {
            //msgAlert("error", "Error", err.message);
            //alert("Error Consultando Información: " + err.message);
            window.error("Error Consultando Información: " + err.message);
        }

        var header = [];

        //$("#" + _IdTable + " thead tr th div").each(function () {
        //    header.push($($(this)[0]).html());
        //})

        $("#" + _IdTable + " thead tr th").each(function () {
            //header.push($($(this)[0]).html());
            header.push($($(this)[0]).text());
        })

        header = header.filter(function (v) { return v !== '' });

        var data = {
            header: header,
            body: []
        };

        for (var i = 0; i < dataex.length; ++i) {
            //for (var i = 1; i < dataex.length; ++i) {
            var rowData = dataex[i];
            var rowDataForExport = [];
            // Get only data of property which display in table
            for (var idx = 0; idx < header.length; ++idx) {
                //for (var idx = 1; idx < header.length; ++idx) {
                var cellData = rowData[header[idx]];
                if (cellData != undefined) {
                    rowDataForExport.push(cellData);
                }
            }
            data.body.push(rowDataForExport);
        }

        return data;
        //}
    });

});

/***MOSTRAR GIF LOADING MIENTRAS REALIZA LAS PETICIONES AJAX***/
$body = $("body");
$(document).on({
    ajaxStart: function () { $body.addClass("loading"); },
    ajaxStop: function () {
        $body.removeClass("loading");

        $('.table').on('click', '.btn-edit', function () {
            var data = _table.row($(this).parents('tr')).data();

            window.location.href = $.url("Clients/Edit/?id=" + data[0]);
        });

        $('#' + _IdTable).DataTable().columns.adjust().responsive.recalc();

        //calculateData(_table);
    }
});

function getClientList(idTable, idGridView) {

    try {
        $.ajax({
            //url: "/Question/getQuestions",
            //url: "/Question/getListQuestions",
            url: $.url("ClientsFilter/getClientList"),
            data: "{}",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data != undefined) {
                    if (data.obj.responseCode >= 0) {
                        //window.success("Configuración actualizada exitosamente");

                        var objdata = $.parseJSON(data.ObjGrid);
                        var mObjData = $.parseJSON(data.ObjData);
                        var objRegister = $.parseJSON(data.listRegister);
                        var objLink = $.parseJSON(data.listLink);
                        var objHidden = $.parseJSON(data.listHidden)
                        var objColumns = (data.columns)
                        PrintGrid(objdata, mObjData, idTable, idGridView, objRegister, objLink, objHidden, objColumns);
                    }
                    else {
                        window.error(data.obj.responseDescription);
                    }
                }
            },
            error: function (xhr) {
                window.error("Error consultando los clientes");
            }
        });
    }
    catch (err) {
        window.error(err.message);
    }
}

function PrintGrid(obj, objData, idTable, idGridView, objRegister, objLink, objHidden, objColumns) {
    var htmlGrid = "";
    var tdclass = "odd";
    var filas;
    var columns;
    var intColumns;
    var encabezados = obj[0];
    var dataIndex = 0;
    var datoFila;
    var bandera = true;

    var indicePosCol;
    var valorColumna;
    var item;
    var swDisplay = true;
    var swRegister = true;
    var intRegister = 0;
    var intLink = 0;
    var swLink = true;
    var display;
    var register = "Visual";

    var NoColumnsVis = [];
    var ColumnsVis = [];
    var intColumnsVis = 0;

    var displayInitColumn = false;

    _objGridJson = obj;
    _objRegister = objRegister;
    _objLink = objLink;
    _objHidden = objHidden;

    var classRegister;
    var classLink;

    //**** COMIENZO CONSTRUCCIÓN TABLE: Declarar ID del Table que tendrá el reporte con sus atributos correspondientes****//
    //htmlGrid = "<table id=\"tblReport\" class=\"table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";

    //htmlGrid = "<table id=\"" + idTable + "\" clientidmode=\"Static\" class=\" hover table table-bordered dt-responsive nowrap\" cellspacing=\"0\" width=\"100%\" aria-describedby=\"example_info\" style=\"width: 100%; background-color: rgb(255, 255, 255);font-size: 14px;\">";
    //htmlGrid = "<table id=\"" + idTable + "\" class=\"hover table table-bordered dt-responsive display\" cellspacing=\"0\" width=\"100%\">";

    htmlGrid = "<table id=\"" + idTable + "\" class=\"hover table table-bordered dt-responsive display order-column " + _nowrap + "\" cellspacing=\"0\" width=\"100%\">";

    //********* Comienza a Armar encabezados********************//
    htmlGrid += "<thead>";
    htmlGrid += "<tr>";
    dataIndex++;

    intColumns = 0;
    var index = [];
    var indice = 0;

    htmlGrid += "<th class=\"dt-center\" style=\"max-width:25px;width:25px;\"></th>";

    intColumns = 0;
    for (i in encabezados) {

        index.push(i)

        if (intColumns != 3) {
            htmlGrid += "<th>" + index[intColumns] + "</th>";
        }
        else {
            htmlGrid += "<th class=\"text-center\">" + index[intColumns] + "</th>";
        }


        intColumns++;

        dataIndex++;
    }
    htmlGrid += "</tr>";
    htmlGrid += "</thead>";
    //********* Fin de armado de encabezados********************//


    //********* Comienza a Armar las filas con todos los registros********************//
    htmlGrid += "<tbody>";

    for (filas = 0; filas < objData.length; filas++) {
        htmlGrid += "<tr role=\"row\">";

        htmlGrid += "<td><button class='btn btn-success btn-xs btn-edit' title='Editar registro'><i class='fa fa-pencil'></i></button> </td>";

        datoFila = objData[filas];

        intColumns = 0;
        for (columns in datoFila) {

            htmlGrid += "<td>";
            htmlGrid += datoFila[columns] + "</td>";

            intColumns++;
        }
        htmlGrid += "</tr>";
    }
    htmlGrid += "</tbody>";
    //********* Fin de Armado de las filas con todos los registros********************//


    htmlGrid += "</table>";
    //**** FIN CONSTRUCCIÓN TABLE****//

    _IdTable = idTable;
    _IdGridView = idGridView;
    _TotalIDRegisters = objRegister.length;

    document.getElementById(idGridView).innerHTML = htmlGrid;

    $("#columns").val(objColumns);
    BuildDataTable(idTable);

    return htmlGrid;
}

function BuildDataTable(idTable) {

    var linkData = true;
    var descriptionColumns = $("#columns").val();
    //var jsonColumns = $.parseJSON(descriptionColumns);
    var jsonColumns = $.parseJSON(descriptionColumns);
    var args = "data, type, full, meta";

    $.each(jsonColumns, function (key, value) {
        var selector = $(this);
        $.each(value, function (key, value) {
            if (key == "render") {
                selector[0].render = new Function(args, value);
            }
        });
    });


    //jsonColumns.render();

    if (_boolResponsive == undefined) {
        _boolResponsive = false;
    }

    var table = $('#' + idTable).DataTable({
        buttons: [
        //{
        //    //extend: 'copyHtml5',
        //    //text: '<a class="btn btn-success btn-sm" id="btnAddQuestion" onclick="document.location.href = $.url(\"QuestionManager/QuestionManager\")"><i class="fa fa-plus-square"></i> Nueva Pregunta</a>',
        //    text: '<i class="newTemplateManage fa fa-plus-square" aria-hidden="true" data-toggle="tooltip" title="Agregar nueva configuración"></i>',
        //    className: 'btn-info',
        //    exportOptions: {
        //        "columns": ':visible',
        //    }
        //},
        {
            extend: 'copyHtml5',
            //extend: 'copy',
            text: '<i class="fa fa-copy fa-lg" aria-hidden="true" data-toggle="tooltip" title="Copiar al Portapapeles"></i>',
            className: 'btn-info',
            exportOptions: {
                "columns": ':visible',
            }
        },
       {
           extend: 'csvHtml5',
           //extend: 'csv',
           text: '<i class="fa fa-table fa-lg" aria-hidden="true" data-toggle="tooltip" title="CSV"></i>',
           fieldSeparator: ';',
           charset: 'UTF-16LE',
           bom: true,
           className: 'btn-info',
           exportOptions: {
               "columns": ':visible',
           }
       },
       {
           extend: 'excelHtml5',
           //extend: 'excel',
           text: ' <i class="fa fa-file-excel-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="Excel"></i>',
           className: 'btn-info',
           exportOptions: {
               "columns": ':visible',
           }
       },
       //{
       //    extend: 'pdfHtml5',
       //    orientation: 'landscape',
       //    pageSize: 'A2',
       //    text: '<i class="fa fa-file-pdf-o fa-lg" aria-hidden="true" data-toggle="tooltip" title="PDF"></i>',
       //    className: 'btn-info'
       //},
       {
           extend: 'colvis',
           columns: '.Visual',
           text: ' <i class="fa fa-eye fa-lg" aria-hidden="true" data-toggle="tooltip" title="Visualizar Columnas"></i>',
           className: 'btn-info'
       },
       //{
       //    text: ' <i class="Responsive fa ' + _Toogle + ' fa-lg active" aria-hidden="true" data-toggle="tooltip" title="Activar/Desactivar Responsive"></i>',
       //    className: 'btn-info btnResponsive'
       //},
        ],

        responsive: _boolResponsive,

        "scrollX": "100%",

        "columns": jsonColumns,
        "iDisplayLength": 50,

        "fnDrawCallback": function (oSettings, json) {

            var headers = _objGridJson[0];

            for (var ind = 0; ind < _objHidden.length; ind++) {
                $(".table td:nth-child(" + ((_objHidden[ind].HIDDEN) + 2) + ")").removeClass("Visual");
                $(".table>thead>tr>th:eq(" + ((_objHidden[ind].HIDDEN) + 1) + ")").removeClass("Visual");
                $(".table td:nth-child(" + ((_objHidden[ind].HIDDEN) + 2) + ")").addClass("NoVisual");
                $(".table>thead>tr>th:eq(" + ((_objHidden[ind].HIDDEN) + 1) + ")").addClass("NoVisual");
            }

            if ((_boolResponsive == false) && _objLink.length == 0) {
                $(".table td:nth-child(1)").removeClass("column0Vis");
                $(".table>thead>tr>th:eq(0)").removeClass("column0Vis");
                $(".table td:nth-child(1)").addClass("NoVisual");
                $(".table>thead>tr>th:eq(0)").addClass("NoVisual");
            }
            else {
                $(".table td:nth-child(1)").removeClass("NoVisual");
                $(".table>thead>tr>th:eq(0)").removeClass("NoVisual");
                $(".table td:nth-child(1)").addClass("column0Vis");
                $(".table>thead>tr>th:eq(0)").addClass("column0Vis");
            }

            $(".NoVisual").css("display", "none");

            $(".table>thead>tr>th:eq(0)").removeClass("sorting_asc");
            $(".table>thead>tr>th:eq(0)").addClass("dt-center");

            $(".table>tbody>tr>td").css("vertical-align", "middle");

            if (_objRegister.length > 0) {
                $(".table>tbody>tr").each(function () {
                    var selector = $(this);
                    var childrenArray = $(selector).children('td');

                    for (var ind = 0; ind < _objRegister.length; ind++) {
                        if ($.trim($(childrenArray[(_objRegister[ind].IDREGISTER) + 1]).text()) == '') {
                            linkData = false;
                            break;
                        }
                    }
                    if (linkData == false) {
                        $(childrenArray[0]).html("");
                        linkData = true;
                    }
                });
            }

            if (_table != undefined && _table != null) {
                calculateData(_table);
            }

        },

        //"searching": false,

        language: {
            sProcessing: "Procesando...",
            sLengthMenu: "Mostrar _MENU_ registros",
            sZeroRecords: "No se encontraron resultados",
            sEmptyTable: "Ningún dato disponible en esta tabla",
            sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix: "",
            sSearch: "Buscar:",
            sUrl: "",
            sInfoThousands: ",",
            sLoadingRecords: "Cargando...",
            oPaginate: {
                sFirst: "Primero",
                sLast: "Último",
                sNext: "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: {
                sSortAscending: ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            },
            buttons: {
                copyTitle: 'Copiado al portapapeles',
                copySuccess: {
                    _: 'Copiados %d registros',
                    1: 'Copiado 1 registro'
                }
            },
            searchPlaceholder: "Buscar registro"
        }

    });

    $($(".row:eq(0)").children()[0]).removeClass("col-sm-6");
    $($(".row:eq(0)").children()[0]).addClass("col-sm-2");

    //$("<div class='col-sm-4'></div>").insertBefore('#' + idTable + '_wrapper .col-sm-6:eq(0)');

    table.buttons().container()
    .appendTo('#' + idTable + '_wrapper .col-sm-6:eq(0)');

    _table = table;
    

    calculateData(_table);
}

function calculateData(table) {
    //data = table
    //.rows()
    //.data();

    data = table
    .rows({ filter: 'applied' }).data();
    //.data();

    averageAge = 0;
    countAge20 = 0;
    countAge50 = 0;

    for (var i = 0; i < data.length; i++) {
        past = new Date(data[i][6]);
        past.setDate(past.getDate() + 1);

        pastYear = past.getFullYear();

        age = nowYear - pastYear;

        averageAge += age

        if (age <= 20) {
            countAge20++;
        }

        if (age > 50) {
            countAge50++;
        }
    }

    averageAge = averageAge / data.length;

    $('#AverageAge').val(averageAge);
    $('#CountAge20').val(countAge20);
    $('#CountAge50').val(countAge50);
}